<?php
/**
 * User: trangdp
 * Date: 07/05/2019
 */

namespace TAI\Lib;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Illuminate\Contracts\Support\Arrayable;

class HttpClientService {
    protected $client;
    
    public function __construct() {
        if ( is_null( $this->client ) ) {
            $this->client = new Client();
        }
    }
    
    
    /**
     * @param       $endpoint
     * @param array $headers
     * @param array $body
     * @return HttpResponse
     * @throws GuzzleException
     */
    public function sendGet( $endpoint, $headers = [], $body = [] ) {
        $request = new Request( 'GET', $endpoint, $headers );
        
        try {
            /** @var Response $response */
            $response = $this->client->send( $request, $body );
            
            if ( !is_null( $response ) ) {
                return new HttpResponse( $response->getStatusCode(), $response->getBody() );
            }
        } catch (RequestException $e) {
            $response = $e->getResponse();
            return new HttpResponse( $response->getStatusCode(), $response->getBody() );
        }
        
    }
    
    /**
     * @param       $endpoint
     * @param array $headers
     * @param array $body
     * @return HttpResponse
     * @throws GuzzleException
     */
    public function sendPost( $endpoint, $headers = [], $body = [] ) {
        
        $request = new Request( 'POST', $endpoint, $headers );
        
        try {
            /** @var Response $response */
            $response = $this->client->send( $request, $body );
            
            if ( !is_null( $response ) ) {
                return new HttpResponse( $response->getStatusCode(), $response->getBody() );
            }
        } catch (RequestException $e) {
            $response = $e->getResponse();
            return new HttpResponse( $response->getStatusCode(), $response->getBody() );
        }
        
    }
}

class HttpResponse implements Arrayable {
    private $statusCode;
    private $data;
    
    public function __construct($statusCode, $data) {
        $this->statusCode = $statusCode;
        $this->data = $data;
    }
    
    /**
     * @return mixed
     */
    public function getStatusCode() {
        return $this->statusCode;
    }
    
    /**
     * @param mixed $statusCode
     */
    public function setStatusCode( $statusCode ): void {
        $this->statusCode = $statusCode;
    }
    
    /**
     * @return mixed
     */
    public function getData() {
        return $this->data;
    }
    
    /**
     * @param mixed $data
     */
    public function setData( $data ): void {
        $this->data = $data;
    }
    
    
    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray() {
        return ["code" => $this->statusCode,
                "data" => $this->data];
    }
}
