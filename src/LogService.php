<?php
/**
 * User: trangdp
 * Date: 07/05/2019
 */

namespace TAI\Lib;

use Illuminate\Support\Facades\Log;

class LogService {
    
    /**
     * caller level:
     * 1nd: getCallFunction()
     * 2nd: log
     * 3rd: _call
     * 4th: the method we want to retrieve
     * @return string|null
     */
    public function getCallFunction() {
        $callers = debug_backtrace( DEBUG_BACKTRACE_IGNORE_ARGS, 4 );
        if (isset($callers[3]))
            return sprintf("%s.%s()", $callers[3]['class'], $callers[3]['function']);
        else return NULL;
    }
    
    
    public function log($message, $data = [], $type = 'debug') {
        Log::{$type}(($this->getCallFunction() ?: __METHOD__). " - $message: ", $data);
    }
    
    //$arguments contain: $message, $data
    public function __call( $name, $arguments ) {
        $numOfItems = count( $arguments );
        $message    = "";
        $data       = [];
        
        if ( $numOfItems > 1 ) {
            $message = $arguments[0];
            $data    = $arguments[1];
        } else if ( $numOfItems == 1 ) {
//            $message = "tracing";
//            $data = $arguments[0];
            if(is_string($arguments[0])) {
                $message = $arguments[0];
                $data = [];
            } else {
                $message = "tracing parameters";
                $data = $arguments[0];
            }
        }
    
        if(is_null($data)) $data = [];
        else if(is_object($data)) $data = (array) $data;
        else $data = [$data];
        
        $this->log($message, $data, $name);
    }
}
