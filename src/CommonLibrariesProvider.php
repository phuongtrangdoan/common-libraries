<?php
/**
 * User: trangdp
 * Date: 08/05/2019
 */

namespace TAI\Lib;


use Illuminate\Support\ServiceProvider;

class CommonLibrariesProvider extends ServiceProvider {
    
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        $this->app->singleton( 'mlog', function ( $app ) {
            return new LogService();
        } );
        
        $this->app->singleton("httpclient", function($app) {
            return new HttpClientService();
        });
    }
}