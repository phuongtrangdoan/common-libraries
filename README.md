**Common Libraries**

This package contains some must-have / optional services / helpers for a Laravel project

---

1. LogService with method name: wrapper from Laravel Log Facade
2. HttpClientService: wrapper from Guzzle lib
---

**How to use**

1. update composer.json, add below :
	```
	"repositories":[
        {
            "type": "vcs",
            "url": "https://phuongtrangdoan@bitbucket.org/phuongtrangdoan/common-libraries.git"
        }
    ],
	require: [
		...
		"trangdp/common-libraries": "dev-master"
	]
	```
2. run composer update
3. add package service provider :

	3.1 for Laravel: update config/app.php 
    
	```
    
    'providers' => [
        ... 
        \Laravel\Common\Libraries\Providers\CommonLibrariesProvider::class,
    ]
    
	```
	
	3.2 For Lumen: update bootstrap/app.php
	```
	$app->register( \Laravel\Common\Libraries\Providers\CommonLibrariesProvider::class );
	```
	
4. In your code:

	4.1 LogService usage:
	```
	app()->make('mlog')->debug("hello world", $array);
	```
	
	or 
	
	```
	app()->make('mlog')->debug($array);
	app()->make('mlog')->debug($object);
	```
	
	4.2 HttpClientService:
	```
	use GuzzleHttp\Psr7\Response;
	....
	
	/** @var Response $response */
	$response = app()->make('httpclient')->sendGet(..$args)
	$response = app()->make('httpclient')->sendPost(...$args)
	```